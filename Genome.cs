﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Genome.cs" company="">
//   
// </copyright>
// <summary>
//   Summary description for Genome.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GeneticAlgorithm
{
    using System;

    /// <summary>
    ///     Summary description for Genome.
    /// </summary>
    /// <remarks>Source: http://www.anotherchris.net/csharp/genetic-algorithms-in-csharp/</remarks>
    public class Genome<T>
    {
        #region Static Fields

        /// <summary>
        /// The m_random.
        /// </summary>
        public static Random m_random = new Random();

        /// <summary>
        /// The m_mutation rate.
        /// </summary>
        private static double m_mutationRate;

        #endregion

        #region Fields

        /// <summary>
        /// The m_genes.
        /// </summary>
        public T[] m_genes;

        /// <summary>
        /// The m_length.
        /// </summary>
        private readonly int m_length;

        public Func<T, T> GenerateMutation;

        public Func<T> GenerateRandom;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Genome"/> class. 
        /// </summary>
        public Genome()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Genome"/> class with a specified length. 
        /// </summary>
        /// <param name="length">
        /// The genome length.
        /// </param>
        public Genome(int length)
        {
            this.m_length = length;
            this.m_genes = new T[length];
            this.RandomizeGenes();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Genome"/> class of a specified length, and initializes
        /// it with random values.
        /// </summary>
        /// <param name="length">
        /// The genome length.
        /// </param>
        /// <param name="randomizeGenes">
        /// true to call <see cref="RandomizeGenes"/>.
        /// </param>
        public Genome(int length, bool randomizeGenes)
        {
            this.m_length = length;
            this.m_genes = new T[length];
            if (randomizeGenes)
            {
                this.RandomizeGenes();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Genome"/> class which is a clone of the specified Genome.
        /// </summary>
        /// <param name="genes">
        /// </param>
        public Genome(ref T[] genes)
        {
            this.m_length = genes.GetLength(0);
            this.m_genes = new T[this.m_length];
            for (int i = 0; i < this.m_length; i++)
            {
                this.m_genes[i] = genes[i];
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Get/set the mutation rate. A value of 0.05 or lower
        ///     is recommended.
        /// </summary>
        public static double MutationRate
        {
            get
            {
                return m_mutationRate;
            }

            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Cant have a mutation rate less than 0.");
                }

                if (value > 1)
                {
                    throw new ArgumentOutOfRangeException("Cant have a mutation rate greater than 1.");
                }

                m_mutationRate = value;
            }
        }

        /// <summary>
        ///     Get/set the fitness of this Genome.
        /// </summary>
        public double Fitness { get; set; }

        /// <summary>
        ///     Returns the length of the Genome.
        /// </summary>
        public int Length
        {
            get
            {
                return this.m_length;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Performs a crossover between this Genome, and another (specified in the 1st argument),
        ///     to produce two children (the 2nd and 3rd arguments).
        /// </summary>
        /// <param name="genome2">
        /// </param>
        /// <param name="child1">
        /// </param>
        /// <param name="child2">
        /// </param>
        public void Crossover(ref Genome<T> genome2, out Genome<T> child1, out Genome<T> child2)
        {
            var pos = (int)(m_random.NextDouble() * this.m_length);
            child1 = new Genome<T>(this.m_length, false);
            child2 = new Genome<T>(this.m_length, false);
            for (int i = 0; i < this.m_length; i++)
            {
                if (i < pos)
                {
                    child1.m_genes[i] = this.m_genes[i];
                    child2.m_genes[i] = genome2.m_genes[i];
                }
                else
                {
                    child1.m_genes[i] = genome2.m_genes[i];
                    child2.m_genes[i] = this.m_genes[i];
                }
            }
        }

        /// <summary>
        /// Returns a deep clone.
        /// </summary>
        /// <returns>
        /// The <see cref="Genome"/>.
        /// </returns>
        public Genome<T> DeepClone()
        {
            var clone = new Genome<T>(this.m_length, false);
            for (int i = 0; i < this.m_length; i++)
            {
                clone.m_genes[i] = this.m_genes[i];
            }

            clone.Fitness = this.Fitness;
            return clone;
        }

        /// <summary>
        /// Returns the genes in a double array.
        /// </summary>
        /// <returns>
        /// The <see cref="T[]"/>.
        /// </returns>
        public T[] Genes()
        {
            return this.m_genes;
        }

        /// <summary>
        /// Copies the values of the genes into a new double array (specified in the 1st arguemnt).
        ///     The is assumed to already be initialized with the correct length.
        /// </summary>
        /// <param name="values">
        /// </param>
        public void GetValues(ref T[] values)
        {
            for (int i = 0; i < this.m_length; i++)
            {
                values[i] = this.m_genes[i];
            }
        }

        /// <summary>
        ///     Mutates the Genome. Genes randomly chosen for mutation will be averaged
        ///     with a new random value between 0 and 1. On average this will tends to
        ///     mutate genes towards 0.5.
        /// </summary>
        public void Mutate()
        {
            for (int pos = 0; pos < this.m_length; pos++)
            {
                if (m_random.NextDouble() < m_mutationRate)
                {
                    this.m_genes[pos] = this.GenerateMutation(this.m_genes[pos]); // (this.m_genes[pos] + m_random.NextDouble()) / 2.0;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Randomizes all the Genomes genes to new values between 0 and 1.
        /// </summary>
        private void RandomizeGenes()
        {
            for (int i = 0; i < this.m_length; i++)
            {
              //  this.m_genes[i] = m_random.NextDouble();
           this.m_genes[i] = this.GenerateRandom();  
            }
        }

        #endregion

        ///// <summary>
        ///// Writes the Genome to the console.
        ///// </summary>
        // public void Output()
        // {
        // for (int i = 0 ; i < m_length ; i++)
        // {
        // System.Console.WriteLine("{0:F4}", m_genes[i]);
        // }
        // System.Console.Write("\n");
        // }
    }
}