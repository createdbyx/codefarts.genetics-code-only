﻿namespace GeneticAlgorithm
{
    using System.Collections.Generic;
                        

    /// <summary>
    /// Compares genomes by fitness
    /// </summary>
    /// <remarks>Source: http://www.anotherchris.net/csharp/genetic-algorithms-in-csharp/</remarks>
    public sealed class GenomeComparer<T> : IComparer<Genome<T>>
    {
        public int Compare(Genome<T> x, Genome<T> y)
        {
            if (x.Fitness > y.Fitness)
                return 1;
            else if (x.Fitness == y.Fitness)
                return 0;
            else
                return -1;
        }
    }
}