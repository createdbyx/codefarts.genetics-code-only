﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GA.cs" company="">
//   
// </copyright>
// <summary>
//   Genetic Algorithm class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GeneticAlgorithm
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    ///     Genetic Algorithm class
    /// </summary>
    /// <remarks>Source: http://www.anotherchris.net/csharp/genetic-algorithms-in-csharp/</remarks>
    public class GA<T>
    {
        #region Static Fields

        /// <summary>
        /// The random.
        /// </summary>
        private static readonly Random random = new Random();

        #endregion

        #region Fields

        /// <summary>
        /// The _fitness table.
        /// </summary>
        private List<double> fitnessTable;

        /// <summary>
        /// The _next generation.
        /// </summary>
        private List<Genome<T>> nextGeneration;

        /// <summary>
        /// The _this generation.
        /// </summary>
        private List<Genome<T>> thisGeneration;

        /// <summary>
        /// The _total fitness.
        /// </summary>
        private double totalFitness;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GA"/> class. 
        ///     Default constructor sets mutation rate to 5%, crossover to 80%, population to 100, and generations to 2000.
        /// </summary>
        public GA()
            : this(0.8, 0.05, 100, 2000, 2)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GA"/> class.
        /// </summary>
        /// <param name="genomeSize">
        /// The genome size.
        /// </param>
        public GA(int genomeSize)
            : this(0.8, 0.05, 100, 2000, genomeSize)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GA"/> class.
        /// </summary>
        /// <param name="crossoverRate">
        /// The crossover rate.
        /// </param>
        /// <param name="mutationRate">
        /// The mutation rate.
        /// </param>
        /// <param name="populationSize">
        /// The population size.
        /// </param>
        /// <param name="generationSize">
        /// The generation size.
        /// </param>
        /// <param name="genomeSize">
        /// The genome size.
        /// </param>
        public GA(double crossoverRate, double mutationRate, int populationSize, int generationSize, int genomeSize)
        {
            this.Elitism = false;
            this.MutationRate = mutationRate;
            this.CrossoverRate = crossoverRate;
            this.PopulationSize = populationSize;
            this.GenerationSize = generationSize;
            this.GenomeSize = genomeSize;
            this.FitnessFile = string.Empty;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the crossover rate.
        /// </summary>
        public double CrossoverRate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether elitism.
        /// </summary>
        public bool Elitism { get; set; }

        /// <summary>
        /// Gets or sets the fitness file.
        /// </summary>
        public string FitnessFile { get; set; }

        /// <summary>
        /// Gets or sets the fitness function.
        /// </summary>
        public Func<T[], double> FitnessFunction { get; set; }

        /// <summary>
        /// Gets or sets the generation size.
        /// </summary>
        public int GenerationSize { get; set; }

        /// <summary>
        /// Gets or sets the genome size.
        /// </summary>
        public int GenomeSize { get; set; }

        /// <summary>
        /// Gets or sets the mutation rate.
        /// </summary>
        public double MutationRate { get; set; }

        /// <summary>
        /// Gets or sets the population size.
        /// </summary>
        public int PopulationSize { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get best.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        /// <param name="fitness">
        /// The fitness.
        /// </param>
        public void GetBest(out T[] values, out double fitness)
        {
            var genome = this.thisGeneration[this.PopulationSize - 1];
            values = new T[genome.Length];
            genome.GetValues(ref values);
            fitness = genome.Fitness;
        }

        /// <summary>
        /// The get nth genome.
        /// </summary>
        /// <param name="n">
        /// The n.
        /// </param>
        /// <param name="values">
        /// The values.
        /// </param>
        /// <param name="fitness">
        /// The fitness.
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// </exception>
        public void GetNthGenome(int n, out T[] values, out double fitness)
        {
            if (n < 0 || n > this.PopulationSize - 1)
            {
                throw new ArgumentOutOfRangeException("n too large, or too small");
            }

            var genome = this.thisGeneration[n];
            values = new T[genome.Length];
            genome.GetValues(ref values);
            fitness = genome.Fitness;
        }

        /// <summary>
        /// The get worst.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        /// <param name="fitness">
        /// The fitness.
        /// </param>
        public void GetWorst(out T[] values, out double fitness)
        {
            this.GetNthGenome(0, out values, out fitness);
        }

        /// <summary>
        ///     Method which starts the GA executing.
        /// </summary>
        public void Go()
        {
            if (this.FitnessFunction == null)
            {
                throw new ArgumentNullException("Need to supply fitness function");
            }

            if (this.GenomeSize == 0)
            {
                throw new IndexOutOfRangeException("Genome size not set");
            }

            // Create the fitness table.
            this.fitnessTable = new List<double>();
            this.thisGeneration = new List<Genome<T>>(this.GenerationSize);
            this.nextGeneration = new List<Genome<T>>(this.GenerationSize);

            Genome<T>.MutationRate = this.MutationRate;
            this.CreateGenomes();
            this.RankPopulation();
            StreamWriter outputFitness = null;
            bool write = false;

            if (this.FitnessFile != string.Empty)
            {
                write = true;
                outputFitness = new StreamWriter(this.FitnessFile);
            }

            for (int i = 0; i < this.GenerationSize; i++)
            {
                this.CreateNextGeneration();
                this.RankPopulation();
                if (write)
                {
                    if (outputFitness != null)
                    {
                        var d = this.thisGeneration[this.PopulationSize - 1].Fitness;
                        outputFitness.WriteLine("{0},{1}", i, d);
                    }
                }
            }

            if (outputFitness != null)
            {
                outputFitness.Close();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Create the *initial* genomes by repeated calling the supplied fitness function
        /// </summary>
        private void CreateGenomes()
        {
            for (int i = 0; i < this.PopulationSize; i++)
            {
                var genome = new Genome<T>(this.GenomeSize);
                this.thisGeneration.Add(genome);
            }
        }

        /// <summary>
        /// The create next generation.
        /// </summary>
        private void CreateNextGeneration()
        {
            this.nextGeneration.Clear();
            Genome<T> genome = null;

            if (this.Elitism)
            {
                genome = this.thisGeneration[this.PopulationSize - 1];
            }

            for (int i = 0; i < this.PopulationSize; i += 2)
            {
                int pidx1 = this.RouletteSelection();
                int pidx2 = this.RouletteSelection();

                Genome<T> parent1, parent2, child1, child2;
                parent1 = this.thisGeneration[pidx1];
                parent2 = this.thisGeneration[pidx2];

                if (random.NextDouble() < this.CrossoverRate)
                {
                    parent1.Crossover(ref parent2, out child1, out child2);
                }
                else
                {
                    child1 = parent1;
                    child2 = parent2;
                }

                child1.Mutate();
                child2.Mutate();

                this.nextGeneration.Add(child1);
                this.nextGeneration.Add(child2);
            }

            if (this.Elitism && genome != null)
            {
                this.nextGeneration[0] = genome;
            }

            this.thisGeneration.Clear();

            for (int i = 0; i < this.PopulationSize; i++)
            {
                this.thisGeneration.Add(this.nextGeneration[i]);
            }
        }

        /// <summary>
        ///     Rank population and sort in order of fitness.
        /// </summary>
        private void RankPopulation()
        {
            this.totalFitness = 0;
            for (int i = 0; i < this.PopulationSize; i++)
            {
                var g = this.thisGeneration[i];
                g.Fitness = this.FitnessFunction(g.Genes());
                this.totalFitness += g.Fitness;
            }

            this.thisGeneration.Sort(new GenomeComparer<T>());

            // now sorted in order of fitness.
            double fitness = 0.0;
            this.fitnessTable.Clear();

            for (int i = 0; i < this.PopulationSize; i++)
            {
                fitness += this.thisGeneration[i].Fitness;
                this.fitnessTable.Add(fitness);
            }
        }

        /// <summary>
        ///     After ranking all the genomes by fitness, use a 'roulette wheel' selection
        ///     method.  This allocates a large probability of selection to those with the
        ///     highest fitness.
        /// </summary>
        /// <returns>Random individual biased towards highest fitness</returns>
        private int RouletteSelection()
        {
            double randomFitness = random.NextDouble() * this.totalFitness;
            int idx = -1;
            int mid;
            int first = 0;
            int last = this.PopulationSize - 1;
            mid = (last - first) / 2;

            // ArrayList's BinarySearch is for exact values only
            // so do this by hand.
            while (idx == -1 && first <= last)
            {
                if (randomFitness < this.fitnessTable[mid])
                {
                    last = mid;
                }
                else if (randomFitness > this.fitnessTable[mid])
                {
                    first = mid;
                }

                mid = (first + last) / 2;

                // lies between i and i+1
                if ((last - first) == 1)
                {
                    idx = last;
                }
            }

            return idx;
        }

        #endregion
    }
}